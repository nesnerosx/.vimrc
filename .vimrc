set nocompatible
syntax on
color wombat

set background=dark
set history=1000
set spell
set shiftwidth=2
set autoindent
set tabstop=4
set softtabstop=4
set ruler
set autoread
set number
set shell=bash
set fileformats=unix
set ff=unix
set backup
set backupdir=~/.vim_backup/
set undolevels=1000
